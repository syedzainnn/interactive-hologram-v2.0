﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFlip : MonoBehaviour {
	public Camera camera;
	Vector3 matr = new Vector3(-1, 1, 1);
	// Use this for initialization
	void Start () {
		
		camera.projectionMatrix = camera.projectionMatrix * Matrix4x4.Scale(matr);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
