﻿using UnityEngine;
using System.Collections;

public class DistanceManager : MonoBehaviour
{
	public GameObject UIInstruction;
	private bool Q1;
	private bool Q2;
	private bool Q3;

	public GameObject QuitWindow;

	private QuizController QC;

	private bool FirstTime ;

	private KinectManager kinectManager;
	int playerIndex = 0;
	private Vector3 UserPosition = new Vector3(0,0,0);
	public GameObject CallMessage;

	void Start()
	{
		CallMessage.SetActive(false);
		QC = GameObject.FindObjectOfType<QuizController>();
		FirstTime = true;
		UIInstruction.SetActive(false);
		kinectManager = KinectManager.Instance;
	}

	void Update()
	{
		if (kinectManager && kinectManager.IsInitialized())
		{

			long userId = kinectManager.GetUserIdByIndex(playerIndex);

			if (kinectManager.IsUserTracked(userId))
			{
				UserPosition = kinectManager.GetUserPosition(userId);
				

				if (Mathf.Abs(UserPosition.z) > 1.30f)
				{
					CallMessage.SetActive(true);

				}
				else if (Mathf.Abs(UserPosition.z) <= 1.25f)

				{
					//if ((QC.Q1 == false) && FirstTime == true )
					if((QC.IsintroMessage == false) && FirstTime == true)
					{
						QC.QuestionHandler();
						FirstTime = false;
					}

				}

				else
				{
					CallMessage.SetActive(false);

				}



			}
			
		}
	}
	
}
