﻿using UnityEngine;

public class Timer : MonoBehaviour
{
	public float timeElapsed;
	public bool StartTimer;

	void Start()
	{
		StartTimer = false;
		timeElapsed = 0f;
	}

	void Update()
	{
		if (StartTimer == true && timeElapsed <= 5f)
		{
			timeElapsed += Time.deltaTime;
		}
		else
		{
			timeElapsed = 0;
			StartTimer = false;
		}

	}

}

