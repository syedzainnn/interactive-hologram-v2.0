﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoroutineTest : MonoBehaviour
{
	WaitForSeconds wait;
	IEnumerator coroutine;
	// Use this for initialization
	void Start()
	{
		wait = new WaitForSeconds(4);
		coroutine = delay();
		StartCoroutine(coroutine);
	}
	IEnumerator delay()
	{
		yield return wait;
		Debug.Log("TEST");
	}


}
