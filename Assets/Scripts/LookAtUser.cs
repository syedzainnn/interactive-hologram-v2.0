﻿using UnityEngine;
using System;


/// <summary>
/// Script that emulates a 3D holographic display based on the viewer position
/// Courtesy of Davy Loots (Twitter: @davloots)
/// - For best effect - and if available - use a stereoscopic display and calculate the head 
///   position twice by simply offsetting the HeadPosition .03 to the left and to the right for
///   each of the views.
/// </summary>
class LookAtUser : MonoBehaviour
{
	[Tooltip("Index of the player, tracked by this component. 0 means the 1st player, 1 - the 2nd one, 2 - the 3rd one, etc.")]
	public int playerIndex = 0;

	//[Tooltip("How high above the ground is the center of the display, in meters.")]
	//public float ScreenCenterY = 0.5f;

	[Tooltip("The position of display center, in Kinect world coordinates, in meters.")]
	public Vector3 screenCenterPos = new Vector3(0f, 1f, 0f);

	[Tooltip("Width of the display in meters.")]
	public float screenWidth = 1.6f; // 0.88f;

	[Tooltip("Height of the display in meters.")]
	public float screenHeight = 0.9f; // 0.50f;

	[Tooltip("Standard distance from the display to the user, in meters.")]
	public float standardUserDistance = 2f;

	[Tooltip("UI-Text to display status messages.")]
	public UnityEngine.UI.Text statusText = null;

	private float left = -0.2F;
	private float right = 0.2F;
	private float bottom = -0.2F;
	private float top = 0.2F;

	private KinectManager kinectManager;

	private Vector3 jointHeadPos;
	private Vector3 headRelPosition;
	private bool headPosValid = false;

	private Vector3 initialUserPos = Vector3.zero;
	private Quaternion initialUserRot = Quaternion.identity;



	void Start()
	{
		kinectManager = KinectManager.Instance;

		GameObject _User = this.gameObject;

		if (_User)
		{
			initialUserPos = _User.transform.position;
			initialUserRot = _User.transform.rotation;
		}
	}

	void Update()
	{
		headPosValid = false;

		if (kinectManager && kinectManager.IsInitialized())
		{
			long userId = kinectManager.GetUserIdByIndex(playerIndex);

			if (kinectManager.IsUserTracked(userId) && kinectManager.IsJointTracked(userId, (int)KinectInterop.JointType.Head))
			{
				jointHeadPos = kinectManager.GetJointPosition(userId, (int)KinectInterop.JointType.Head);
				headRelPosition = jointHeadPos - screenCenterPos;
				headPosValid = true;

				if (statusText)
				{
					string sStatusMsg = string.Format("Head position: {0}\nRelative to screen: {1}", jointHeadPos, headRelPosition);
					statusText.text = sStatusMsg;
				}
			}
			else
			{
			}
		}

	}


	/// <summary>
	/// Updates the projection matrix and camera position to get the correct anamorph perspective
	/// </summary>
	void LateUpdate()
	{
		GameObject _user = this.gameObject;

		if (_user)
		{
			if (headPosValid)
			{
				//_user.transform.position = new Vector3(headRelPosition.x, headRelPosition.y, -headRelPosition.z);


				//_user.transform.LookAt(new Vector3(headRelPosition.x, headRelPosition.y, 0));
				_user.transform.LookAt(new Vector3(-jointHeadPos.x, jointHeadPos.y, 0));

			}
			else
			{
				// set the initial camera settings
				_user.transform.position = initialUserPos;
				_user.transform.rotation = initialUserRot;
			}
		}
	}

}

