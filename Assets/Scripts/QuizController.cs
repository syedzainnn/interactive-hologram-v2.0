﻿using System.Collections;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class QuizController : MonoBehaviour
{

	#region Variables Declaration

	private WaitForSeconds _waitforThree;
	private WaitForSeconds _waitforSix;
	private IEnumerator delayCoroutine;
	KinectManager kM;
	
	// Question Booleans
	private bool IsQuit_;
	private bool IsEnding;
	public bool IsintroMessage;
	public bool NoUsers;

	// Button Booleans
	private bool Button_NO, Button_YES;
	private bool Button_Agree, Button_Disagree;
	private bool Button_Believe, Button_Disbelieve;
	private bool Button_IThinkSo, Button_IDontThinkSo;
	private bool Button_SecondThoughts, Button_ShouldHave;


	//Questions UI
	public GameObject Question1;
	public GameObject Question2A;
	public GameObject Question2B;
	public GameObject Question3A1, Question3A2;
	public GameObject QuitWindow;
	public GameObject FinalStatement;
	public GameObject CallMessage;

	public GameObject Particles_Yes, Particles_NO, Particles_Agree, Particles_Disagree, 
	Particles_Believe, Particles_Disbelieve, Particles_IThinkSo, Particles_IDontThinkSo, 
	Particles_ShouldHave, Particles_SecondThoughts;


	public Sounds[] Raffles_VO;
	AudioSource _AudioSource;
	#endregion



	private void Awake()
	{
		_waitforThree = new WaitForSeconds(3);
		_waitforSix = new WaitForSeconds(10);
		_AudioSource = GetComponent<AudioSource>();

		foreach (Sounds s in Raffles_VO)
		{
			s.source = _AudioSource;
			s.source.volume = s.volume;
		}
	}


	void Start()
	{

		IsQuit_ = IsintroMessage = IsEnding = false;

		Button_NO = Button_YES  =  Button_Agree = Button_Disagree =
	    Button_Believe = Button_Disbelieve = Button_IThinkSo = 
		Button_IDontThinkSo = Button_SecondThoughts = Button_SecondThoughts = false;


		Question1.SetActive(false);
		QuitWindow.SetActive(false);
		Question2A.SetActive(false);
		Question2B.SetActive(false);
		Question3A1.SetActive(false);
		Question3A2.SetActive(false);
		FinalStatement.SetActive(false);

		Particles_Yes.SetActive(false);
		Particles_NO.SetActive(false);
		Particles_Agree.SetActive(false);
		Particles_Disagree.SetActive(false);
		Particles_Believe.SetActive(false);
		Particles_Disbelieve.SetActive(false);
		Particles_IThinkSo.SetActive(false);
		Particles_IDontThinkSo.SetActive(false);
		Particles_SecondThoughts.SetActive(false);
		Particles_ShouldHave.SetActive(false);

	}

	#region Question enums
	IEnumerator SpeechOne_Delay()
	{
		IsintroMessage = true;
		_AudioSource.clip = Raffles_VO[0].clip;
		_AudioSource.Play();
		yield return new WaitWhile(() => _AudioSource.isPlaying == true);
		Question1.SetActive(true);
	}

	IEnumerator QuestionOne_Handle()
	{
		yield return _waitforThree;
		
		if (Button_YES)
		{
			Particles_Yes.SetActive(false);
			Question1.SetActive(false);
			_AudioSource.clip = Raffles_VO[1].clip;
			_AudioSource.Play();
			yield return new WaitWhile(() => _AudioSource.isPlaying == true);

			Question2A.SetActive(true);
			Button_YES = false;
		}

		else if (Button_NO)
		{
			Particles_NO.SetActive(false);
			Question1.SetActive(false);
			_AudioSource.clip = Raffles_VO[2].clip;
			_AudioSource.Play();
			yield return new WaitWhile(() => _AudioSource.isPlaying == true);
			Question2B.SetActive(true);
			Button_NO = false;

		}
	}
IEnumerator QuestionTwo_Handle ()
	{
		yield return _waitforThree;
		if (Button_Agree)
		{
			Particles_Agree.SetActive(false);
			Question2A.SetActive(false);
			_AudioSource.clip = Raffles_VO[3].clip;
			_AudioSource.Play();
			yield return new WaitWhile(() => _AudioSource.isPlaying == true);
			Question3A1.SetActive(true);
			Button_Agree = false;
		}

		else if (Button_Disagree)
		{
			Particles_Disagree.SetActive(false);
			Question2A.SetActive(false); 
			_AudioSource.clip = Raffles_VO[4].clip;
			_AudioSource.Play();
			yield return new WaitWhile(() => _AudioSource.isPlaying == true);
			Question3A2.SetActive(true);
			Button_Disagree = false;
		}

		else if (Button_Believe)
		{
			Particles_Believe.SetActive(false);
			Question2B.SetActive(false);
			_AudioSource.clip = Raffles_VO[5].clip;
			_AudioSource.Play();
			yield return new WaitWhile(() => _AudioSource.isPlaying == true);
			Question2A.SetActive(true);
			Button_Believe = false;
		}

		else if (Button_Disbelieve)
		{
			Particles_Disbelieve.SetActive(false);
			Question2B.SetActive(false);
			_AudioSource.clip = Raffles_VO[6].clip;
			_AudioSource.Play();
			yield return new WaitWhile(() => _AudioSource.isPlaying == true);
			IsEnding = true;
			yield return _waitforSix;
			delayCoroutine = SpeechOne_Delay();
			//StartCoroutine(delayCoroutine);
			yield return delayCoroutine;
			Button_Disbelieve = false;
	
		}
	}

	IEnumerator QuestionThree_Handle()
	{
		yield return _waitforThree;
		if(Button_IThinkSo)
		{
			Particles_IThinkSo.SetActive(false);
			Question3A1.SetActive(false);
			_AudioSource.clip = Raffles_VO[7].clip;
			_AudioSource.Play();
			yield return new WaitWhile(() => _AudioSource.isPlaying == true);
			FinalStatement.SetActive(true);

			IsEnding = true;
			yield return _waitforSix;
			FinalStatement.SetActive(false);
			delayCoroutine = SpeechOne_Delay();
			//StartCoroutine(delayCoroutine);
			yield return delayCoroutine;
			Button_IThinkSo = false;
			
		}

		else if (Button_IDontThinkSo)
		{
			Particles_IDontThinkSo.SetActive(false);
			Question3A1.SetActive(false);
			_AudioSource.clip = Raffles_VO[8].clip;
			_AudioSource.Play();
			yield return new WaitWhile(() => _AudioSource.isPlaying == true);
			FinalStatement.SetActive(true);

			IsEnding = true;

			yield return _waitforSix;
			FinalStatement.SetActive(false);
			delayCoroutine = SpeechOne_Delay();
			//StartCoroutine(delayCoroutine);
			yield return delayCoroutine;
			Button_IDontThinkSo = false;
		
		}

		else if (Button_SecondThoughts)
		{
			Particles_SecondThoughts.SetActive(false);
			Question3A2.SetActive(false);
			_AudioSource.clip = Raffles_VO[9].clip;
			_AudioSource.Play();
			yield return new WaitWhile(() => _AudioSource.isPlaying == true);
			FinalStatement.SetActive(true);

			IsEnding = true;
			yield return _waitforSix;
			FinalStatement.SetActive(false);
			delayCoroutine = SpeechOne_Delay();
			//StartCoroutine(delayCoroutine);
			yield return delayCoroutine;
			Button_SecondThoughts = false;

			
		}
		else if (Button_ShouldHave)
		{
			Particles_ShouldHave.SetActive(false);
			Question3A2.SetActive(false);
			_AudioSource.clip = Raffles_VO[10].clip;
			_AudioSource.Play();
			yield return new WaitWhile(() => _AudioSource.isPlaying == true);
			FinalStatement.SetActive(true);

			IsEnding = true;
			yield return _waitforSix;
			FinalStatement.SetActive(false);
			delayCoroutine = SpeechOne_Delay();
			//StartCoroutine(delayCoroutine);
			yield return delayCoroutine;
			Button_ShouldHave = false;
		
		}
	}

	#endregion

	public void QuestionHandler()
	{
		delayCoroutine = SpeechOne_Delay();
		StartCoroutine(delayCoroutine);
	}

	#region Button Handle Functions

	public void OnYes()
	{
		if (Question1.activeSelf == true)
		{
			Button_YES = true;
			delayCoroutine = QuestionOne_Handle();
			StartCoroutine(delayCoroutine);
		}
	}

	public void _OnNo()
	{
		if (Question1.activeSelf == true)
		{
			Button_NO = true;
			delayCoroutine = QuestionOne_Handle();
			StartCoroutine(delayCoroutine);
		}
	}


	public void OnAgree()
	{
		if (Question2A.activeSelf == true)
		{
			Button_Agree = true;
			delayCoroutine = QuestionTwo_Handle();
			StartCoroutine(delayCoroutine);
		}
	}

	public void OnDisagree()
	{
		if (Question2A.activeSelf == true)
		{
			Button_Disagree = true;
			delayCoroutine = QuestionTwo_Handle();
			StartCoroutine(delayCoroutine);
		}
	}


	public void OnBelieve()
	{
		if (Question2B.activeSelf == true)
		{
			Button_Believe = true;
			delayCoroutine = QuestionTwo_Handle();
			StartCoroutine(delayCoroutine);
		}
	}

	public void OnDisbelieve()
	{
		if (Question2B.activeSelf == true)
		{
			Button_Disbelieve = true;
			delayCoroutine = QuestionTwo_Handle();
			StartCoroutine(delayCoroutine);
		}
	}

	public void OnIThinkSo()
	{
		if (Question3A1.activeSelf == true)
		{
			Button_IThinkSo = true;
			delayCoroutine = QuestionThree_Handle();
			StartCoroutine(delayCoroutine);
		}
	}

	public void OnIDontThinkSo()
	{
		if (Question3A1.activeSelf == true)
		{
			Button_IDontThinkSo = true;
			delayCoroutine = QuestionThree_Handle();
			StartCoroutine(delayCoroutine);
		}
	}

	public void OnSecondThoughts()
	{
		if (Question3A2.activeSelf == true)
		{
			Button_SecondThoughts = true;
			delayCoroutine = QuestionThree_Handle();
			StartCoroutine(delayCoroutine);
		}

	}

	public void OnShouldHave()
	{
		if (Question3A2.activeSelf == true)
		{
			Button_ShouldHave = true;
			delayCoroutine = QuestionThree_Handle();
			StartCoroutine(delayCoroutine);
		}

	}
	public void Terminate()
	{
		if (IsEnding)
		{
			if (NoUsers == true)
			{
				SceneManager.LoadScene("FinalScene");
			}


		}
	}


	#endregion
}
