﻿using UnityEngine;
using UnityEngine.UI;

public class SpeechController : MonoBehaviour, SpeechRecognitionInterface
{
	//private SpeechManager speechManager;
	public Button YES_BUTTON, NO_BUTTON, AGREE_BUTTON, DISAGREE_BUTTON,
	BELIEVE_BUTTON, DISBELIEVE_BUTTON, ITHINKSO_BUTTON, IDONTHINKSO_BUTTON, 
	SECONDTHOUGHTS_BUTTON, SHOULDHAVE_BUTTON;

	public bool SpeechPhraseRecognized(string phraseTag, float condidence)
	{
		switch (phraseTag)
		{

			case "YES YOU WERE":
				Debug.Log("YES YOU WERE");
				YES_BUTTON.onClick.Invoke();
				break;

			case "NO YOU WERE NOT":
				Debug.Log("NO YOU WERE NOT");
				NO_BUTTON.onClick.Invoke();
				break;

			case "YES I AGREE":
				Debug.Log("YES I AGREE");
				AGREE_BUTTON.onClick.Invoke();
				break;

			case "NO I DO NOT AGREE":
				Debug.Log("NO I DO NOT AGREE");
				DISAGREE_BUTTON.onClick.Invoke();
				break;

			case "YES I BELIEVE NOW YOU WERE":
				Debug.Log("YES I BELIEVE NOW YOU WERE");
				BELIEVE_BUTTON.onClick.Invoke();
				break;

			case "NO I STILL DO NOT BELIEVE":
				Debug.Log("NO I STILL DO NOT BELIEVE");
				DISBELIEVE_BUTTON.onClick.Invoke();

				break;

			case "YES I THINK SO":
				Debug.Log("YES I THINK SO");
				ITHINKSO_BUTTON.onClick.Invoke();
				break;

			case "NO I DON'T THINK SO":
				Debug.Log("NO I DON'T THINK SO");
				IDONTHINKSO_BUTTON.onClick.Invoke();
				break;

			case "SECOND THOUGHTS NO":
				Debug.Log("SECOND THOUGHTS NO");
				SECONDTHOUGHTS_BUTTON.onClick.Invoke();
				break;

			case "YES YOU SHOULD HAVE":
				Debug.Log("YES YOU SHOULD HAVE");
				SHOULDHAVE_BUTTON.onClick.Invoke();
				break;

		}

		return true;
	}

}
