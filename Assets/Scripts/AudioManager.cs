﻿using System.Collections;
using UnityEngine.Audio;
using System.Collections.Generic;
using UnityEngine;
using System;
public class AudioManager : MonoBehaviour
{
	public Sounds[] Raffles_VO;
	AudioSource _audio;
	AudioSource _audioSource;
	int _index = 0;
	Sounds S;


	void Awake()
	{

		_audioSource = GetComponent<AudioSource>();
		//S = Raffles_VO[_index];
		foreach (Sounds s in Raffles_VO)
		{
			s.source = _audioSource;
			s.source.volume = s.volume;
		}



		for (int i = 0; i < Raffles_VO.Length; i++)
		
		{
			_audioSource.Play();
			StartCoroutine(WaitForSound(_audioSource));
			//_audioSource.Play();

		}
	}




	public  void PlayAudio()
	{
		_audio = GetComponent<AudioSource>();
		_audio.Play();

	}


	public void RafflesIncrementalAudio()
	{
		Sounds s = Raffles_VO[_index];
		s.source.clip = s.clip;
		s.source.Play();
		if (_index <= Raffles_VO.Length)
		{
			//StartCoroutine(WaitForSound(s));
		}
		
	}

	public IEnumerator WaitForSound(AudioSource _as)
	{
		S = Raffles_VO[_index];
		_as.clip = S.clip;

		//S.source.clip = S.clip;
		//yield return new WaitUntil(() => _as.isPlaying == false);
		yield return new WaitWhile(() => _as.isPlaying == true);
		_as.Play();
		
	}

}
